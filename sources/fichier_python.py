from tkinter import *
import time
import tkinter as tk
##########################################################
############# Fonctions ##################################
##########################################################
def fctDroite(ev=None):
    global image  
    global tours
    global temps
    global direction
    global L



    a, b, c, d = Canevas.coords(image)
    i = int((a - 100) / 50)
    j = int((b - 100) / 50)
   
    if L[j][i + 1] == 2:
        time.sleep(2)  # Ralentissement sur la case 2
        Canevas.coords(image, a + 50, b, c + 50, d)
    if L[j][i + 1] == 7:  # Si la case suivante est le départ, incrémenter les tours
        tours += 1
        update_tours_label()
    if L[j][i + 1] == 5:  # Si la case suivante est orange, revenir à la position [17, 18]
        Canevas.coords(image, 900, 950, 950, 1000)
    

def fctGauche(ev=None):
    global image
    global tours
    global temps
    global direction
    global L

   

    a, b, c, d = Canevas.coords(image)
    i = int((a - 100) / 50)
    j = int((b - 100) / 50)
    
    if L[j][i - 1] == 2:
        time.sleep(2)  # Ralentissement sur la case 2
        Canevas.coords(image, a - 50, b, c - 50, d)
    if L[j][i - 1] == 7:  # Si la case suivante est le départ, incrémenter les tours
        tours += 1
        update_tours_label()
    if L[j][i - 1] == 5:  # Si la case suivante est orange, revenir à la position [17, 18]
        Canevas.coords(image, 900, 950, 950, 1000)
    

def fctBas(ev=None):
    global image
    global tours
    global temps
    global direction
    global L

    
    a, b, c, d = Canevas.coords(image)
    i = int((a - 100) / 50)
    j = int((b - 100) / 50)

    if L[j + 1][i] == 2:
        time.sleep(2)  # Ralentissement sur la case 2
        Canevas.coords(image, a, b + 50, c, d + 50)
    if L[j + 1][i] == 7:  # Si la case suivante est le départ, incrémenter les tours
        tours += 1
        update_tours_label()
    if L[j + 1][i] == 5:  # Si la case suivante est orange, revenir à la position [17, 18]
        Canevas.coords(image, 900, 950, 950, 1000)
   

def fctHaut(ev=None):
    global image
    global tours
    global temps
    global direction
    global L

    

    a, b, c, d = Canevas.coords(image)
    i = int((a - 100) / 50)
    j = int((b - 100) / 50)

    if L[j - 1][i] == 2:
        time.sleep(2)  # Ralentissement sur la case 2
        Canevas.coords(image, a, b - 50, c, d - 50)
    if L[j - 1][i] == 7:  # Si la case suivante est le départ, incrémenter les tours
        tours += 1
        update_tours_label()
    if L[j - 1][i] == 5:  # Si la case suivante est orange, revenir à la position [17, 18]
        Canevas.coords(image, 900, 950, 950, 1000)
    

def Rebours(tc):
    mins, secs = divmod(tc, 60)
    timer = '{:02d}:{:02d}'.format(mins, secs)
    temps_label['text'] = timer
    if tc:
        Mafenetre.after(1000, lambda: Rebours(tc-1))
    else:
        temps_label['text'] = "Go"
        Mafenetre.focus_set()

def demarrer_rebours():
    temps_label['text'] = "Décompte : 3"
    Mafenetre.after(1000, lambda: temps_label.config(text="Décompte : 2"))
    Mafenetre.after(2000, lambda: temps_label.config(text="Décompte : 1"))
    Mafenetre.after(3000, lambda: Rebours(60))


def check_finish():
    global temps
    global tours
    global image
    # Vérifie si la boule est sur la case de fin (indice 7)
    [a, b, c, d] = Canevas.coords(image)
    i = int((a - 100) / 50)
    j = int((b - 100) / 50)
    if L[j][i] == 7:
        tours += 1
        update_tours_label()

        # Vérifie si le joueur a terminé 3 tours
        if tours == 3:
            temps_fin = time.time()
            temps_total = temps_fin - temps_debut
            temps = round(temps_total, 2)
            message_label['text'] = "Bravo ! Vous avez terminé la course en {} secondes avec succès !".format(temps)
            bouton_recommencer.grid(row=4, column=0, columnspan=2)
            Canevas.unbind_all('<d>')
            Canevas.unbind_all('<q>')
            Canevas.unbind_all('<s>')
            Canevas.unbind_all('<z>')
            


def recommencer():
    global temps_debut
    global temps
    global tours
    global temps_label
    global message_label
    global bouton_recommencer

    # Réinitialisation des variables
    temps_debut = time.time()
    temps = 0
    tours = 0
    temps_label['text'] = "Temps: 00:00"
    message_label['text'] = ""
    bouton_recommencer.grid_forget()

    # Réinitialisation de la grille et de la boule
    a = 0
    i = 0
    while i < 20:
        b = 0
        j = 0
        while j < 20:
            if L[j][i] == 1:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='black')
            elif L[j][i] == 0:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='white')
            elif L[j][i] == 2:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='green')
            elif L[j][i] == 3:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='red')
            elif L[j][i] == 4:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='grey')
            elif L[j][i] == 5:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='orange')
            elif L[j][i] == 6:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='blue')
            elif L[j][i] == 7:
                Canevas.create_rectangle(a, b, a + 50, b + 50, fill='red')
            b += 50
            j += 1
        a += 50
        i += 1
    # Replace la boule au départ
    image=Canevas.create_oval(850,850,900,900,fill='red')
    label['text'] = "Go"
    Rebours(3)  # Recommence le décompte
def update_tours_label():
    tours_label['text'] = "Tours: {}".format(tours)
    
def afficher_grille(L, Canevas):
    """Affiche la grille L dans le canevas."""
    for i in range(len(L)):
        for j in range(len(L[i])):
            if L[i][j] == 0:
                couleur = 'white'
            elif L[i][j] == 1:
                couleur = 'black'
            elif L[i][j] == 2:
                couleur = 'green'
            elif L[i][j] == 3:
                couleur = 'red'
            elif L[i][j] == 4:
                couleur = 'grey'
            elif L[i][j] == 5:
                couleur = 'orange'
            elif L[i][j] == 6:
                couleur = 'blue'
            elif L[i][j] == 7:
                couleur = 'red'
            Canevas.create_rectangle(j * 50, i * 50, (j + 1) * 50, (i + 1) * 50, fill=couleur)
            
def afficher_temps():
    """Affiche le temps restant après le décompte."""
    global temps
    mins, secs = divmod(temps, 60)
    temps_affiche = '{:02d}:{:02d}'.format(mins, secs)
    temps_label['text'] = "Temps: " + temps_affiche

def recommencer():
    """Fonction pour recommencer la course."""
    global temps_debut, temps, tours, message_label, bouton_recommencer
    
    # Réinitialisation des variables
    temps_debut = time.time()
    temps = 0
    tours = 0
    afficher_temps()  # Met à jour l'affichage du temps
    tours_label['text'] = "Tours: {}".format(tours)
    message_label['text'] = ""
    bouton_recommencer.grid_forget()

    # Réinitialisation de la grille et de la boule
    Canevas.delete("all")
    afficher_grille(L, Canevas)
    image = Canevas.create_oval(850, 850, 900, 900, fill='red')
    label['text'] = "Go"
    Rebours(3)  # Recommence le décompte
    

##########################################################
############# Variables ##################################
##########################################################
tours = 0
temps = 0
temps_debut = time.time()

L=[[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
   [4,2,2,2,2,2,0,0,0,0,0,0,2,2,2,2,2,2,2,4],
   [4,2,2,5,0,0,0,0,5,5,0,0,0,0,0,2,2,2,2,4],
   [4,2,5,0,0,5,5,5,5,5,5,5,5,5,0,0,2,2,2,4],
   [4,2,5,5,0,0,0,0,0,0,0,0,0,5,5,0,2,2,2,4],
   [4,2,2,2,2,2,2,2,2,2,2,2,0,0,2,0,5,2,2,4],
   [4,2,2,0,0,0,0,0,0,0,0,0,0,2,2,0,0,2,2,4],
   [4,2,0,0,2,2,2,2,2,2,2,2,2,2,2,2,0,2,2,4],
   [4,2,2,0,0,0,0,0,0,0,0,0,0,2,2,2,0,2,2,4],
   [4,2,5,2,2,2,2,2,2,2,2,2,0,0,2,2,0,2,2,4],
   [4,5,5,0,0,0,0,0,0,0,0,2,2,0,2,0,0,2,2,4],
   [4,2,0,0,5,2,2,2,2,2,0,0,2,0,2,0,2,2,2,4],
   [4,2,0,2,2,2,2,2,2,2,2,0,2,0,2,0,0,0,2,4],
   [4,2,0,2,2,2,0,2,2,2,2,0,0,0,2,2,2,0,0,4],
   [4,2,0,2,2,0,0,0,2,2,2,2,0,2,2,2,2,2,0,4],
   [4,2,0,2,2,0,2,0,2,2,2,2,2,2,2,2,2,2,0,4],
   [4,2,0,0,2,0,2,0,0,2,2,2,2,2,2,2,2,0,0,4],
   [4,2,5,0,0,0,2,2,0,0,0,0,0,0,0,0,0,7,2,4],
   [4,2,5,5,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4],
   [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]]


#########################################################
########## Interface graphique ##########################
##########################################################
Mafenetre = Tk()
Mafenetre.title("Course de voitures")

Canevas = Canvas(Mafenetre, width=1200, height=1100, bg='white')
Canevas.pack()

label = Label(Mafenetre, text="Go", font=("helvetica", 40))
label.place(x=50, y=50)

temps_label = Label(Mafenetre, text="Temps: 00:00", font=("helvetica", 20))
temps_label.place(x=50, y=100)

tours_label = Label(Mafenetre, text="Tours: 0", font=("helvetica", 20))
tours_label.place(x=50, y=150)

message_label = Label(Mafenetre, text="", font=("helvetica", 20))
message_label.place(x=50, y=200)

bouton_recommencer = Button(Mafenetre, text="Recommencer", command=recommencer)
bouton_recommencer.place(x=50, y=250)
Rebours(3) # Rebours mis à 3 secondes

# Affichage de la grille après la création du canevas
afficher_grille(L, Canevas)

image = Canevas.create_oval(850, 850, 900, 900, fill='red')



# Création du label pour afficher le temps
temps_label = tk.Label(Mafenetre, text="Appuyez sur Démarrer", font=("Helvetica", 24))
temps_label.pack(pady=20)

# Création du bouton pour démarrer le compte à rebours
demarrer_bouton = Button(Mafenetre, text="Démarrer", command=demarrer_rebours)
demarrer_bouton.place(x=50, y=300)
demarrer_bouton.pack()


###########################################################
########### Receptionnaire d'évènement ####################
###########################################################
Canevas.bind_all('<d>', fctDroite)
Canevas.bind_all('<q>', fctGauche)
Canevas.bind_all('<s>', fctBas)
Canevas.bind_all('<z>', fctHaut)

##########################################################
###################### FIN ###############################
##########################################################
Mafenetre.mainloop()
##########################################################
##################### ORGANISATION ######################
##########################################################
